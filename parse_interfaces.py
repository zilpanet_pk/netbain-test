import re
import json


def parse_interface():
    output = []
    with open('router_test_output.txt', 'r') as file:
        content = file.read()  # read the file contents
        pattern = re.compile('(^interface.*?)!$', re.DOTALL | re.M)
        data = pattern.findall(content)  # break all the interfaces configuration
        for item in data:
            interface = re.findall(pattern=r"^interface\s(.+)", string=item)
            desc = re.findall(pattern=r'description(.+)', string=item)
            ip_address = re.findall(
                pattern='ip address\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})',
                string=item)
            status = re.findall(pattern=r"(shutdown)", string=item)

            ipaddresses = []
            if ip_address:
                for value in ip_address:
                    get_mask = value[1]
                    mask = sum(
                        [bin(int(bits)).count("1") for bits in get_mask.split(".")])  # convert subnet mask to CIDR
                    format_ = value[0] + '/' + str(mask)  # formatting the ip and subnet mask ex 192.168.1.10/24
                    ipaddresses.append(format_)
            if not desc:
                desc = ""
            else:
                desc = ''.join(desc)  # if description is defined convert it to string
            if status:
                status = "shutdown"  # if shutdown in status
            else:
                status = "up"
            payload = {
                "name": interface[0],
                "description": desc,
                "ip_addresses": ipaddresses,
                "status": status

            }
            output.append(payload)  # append output to list
    return output


call = parse_interface()
print(json.dumps(call, indent=4))
