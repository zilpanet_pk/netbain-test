import re
import json


class getRoutes:

    def get_imports_exports(self, content): #this function parses import and export routes of last vrf
        last_item = len(content) - 1 
        req_data = ''
        n = """
            {}
            """.format(content[last_item])
        count = 0
        for line in n.split('\n'):
            if '!' in line:
                count += 1
            if count > 1:
                break
            elif count != 0:
                req_data = req_data + line + '\n'
        last_export = re.findall('(?<=route-target export )(.*)', req_data)
        last_import = re.findall('(?<=route-target import )(.*)', req_data)
        return last_export, last_import

    def export_routes(self, content): #this functions parses the vrf values  from each config
        data1 = re.split("vrf definition", content) #caputing all vrf configuarion
        call_func = self.get_imports_exports(data1) 

        route_export = []
        route_import = []

        for i in data1[1:]: #traverse all the vrfs and caputure export import routes
            exp_route = re.findall(r"route-target export.(\S+)", i)
            imp_route = re.findall(r"route-target import.(\S+)", i)
            route_export.append(exp_route)
            route_import.append(imp_route)

        last_exports = call_func[0]
        route_export.pop()
        route_export.append(last_exports)
        last_imports = call_func[1]
        route_import.pop()
        route_import.append(last_imports)
        return route_export, route_import


with open('router_test_output.txt', 'r') as file:
    content = file.read()
    compile_reg = re.compile(
        pattern=r"interface\s(\S+)\n.+?\n?vrf forwarding\s(\S+)?|interface\s(\S+)\n.+?\n\svrf forwarding\s(\S+)")
    output = re.findall(compile_reg, string=content)
    store = []
    for element in output:
        getList = list(element) #get all interfaces associtated to vrfs
        interface_vrf = [item for item in getList if item]
        payload = {
            "interface": interface_vrf[0],
            "vrf": interface_vrf[1]
        }
        store.append(payload)
    find_router_id = re.compile(pattern=r"vrf definition\s(.*)\n\s(rd\s(.+))?") #capture router ids
    output = []
    match = re.findall(pattern=find_router_id, string=content)
    obj = getRoutes()
    call = obj.export_routes(content)

    for tuple, export, imports in zip(match, call[0], call[1]):
        data = {
            "name": tuple[0],
            "router": tuple[2],
            "interface": [i["interface"] for i in store if i['vrf'] == tuple[0]],
            "target_values": {
                "export": export,
                "import": imports
            }
        }
        output.append(data)

print(json.dumps(output, indent=4))
